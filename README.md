# dast-callback-attacks

This project is a live example of a CI pipeline which has followed the steps in the Breach and Attack Simulation documentation to [Extend Dynamic Application Security Testing (DAST)](https://docs.gitlab.com/ee/user/application_security/breach_and_attack_simulation/#extend-dynamic-application-security-testing-dast) with BAS's callback attack implementation.

Additionally it shows how you can leverage service containers for running [callback](https://gitlab.com/gitlab-org/incubation-engineering/breach-and-attack-simulation/security-products/callback) as the job's HTTP callback server.

See [.gitlab-ci.yml](./.gitlab-ci.yml) for up to date details on how this pipeline runs. You can create your own copy of the project on GitLab.com if you have an Ultimate associated with the group/user you'd like to test running a BAS pipeline with.

Various CVEs or CWEs such as CWE-94, CWE-601, CWE-611, to name a few can use a callback server to prove the impact of these weaknesses/vulnerabilities leading to successful exploitation.
